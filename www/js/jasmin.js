/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */



function dropdownFunction() {
	document.getElementById("dropdown-contentID2").classList.remove('show');
	document.getElementById("dropdown-contentID").classList.toggle("show");
	}

function dropdownFunction2() {
	document.getElementById("dropdown-contentID").classList.remove('show');
	document.getElementById("dropdown-contentID2").classList.toggle("show");
	}

// Close the dropdown menu if the user clicks outside of it

window.onclick = function(closeEvent) {
  if (!closeEvent.target.matches('.divButtonClass')) {

    var dropContentLinks1 = document.getElementsByClassName("dropdown-content1");
	var i;
    for (i = 0; i < dropContentLinks1.length; i++) {
      var openDropdown1 = dropContentLinks1[i];
      console.log(openDropdown1);
      if (openDropdown1.classList.contains('show')) {
        openDropdown1.classList.remove('show');
      }
    }
  }
  if (!closeEvent.target.matches('.divButtonClass2')) {

  	var dropContentLinks2 = document.getElementsByClassName("dropdown-content2");
	var i2;

    for (i2 = 0; i2 < dropContentLinks2.length; i2++) {
      var openDropdown2 = dropContentLinks2[i2];
      console.log(openDropdown2);
      if (openDropdown2.classList.contains('show')) {
        openDropdown2.classList.remove('show');
      }
    }
  }
}



var head = false;
var body = false;



function dropdownHeadSelect(animalName) {

	head = animalName;
	breedButtonDisplayFunction();

	var animalList = document.getElementsByClassName("preview_head_animal")

	for (var i = 0; i < animalList.length; i ++) {


		animalList[i].style.display = "none";
	}
	
	document.getElementById('preview_'+ animalName + '1').style.display = "block";


	};





function dropdownBodySelect(animalName) {
	body = animalName;
	breedButtonDisplayFunction();

	var animalList = document.getElementsByClassName("preview_body_animal")

	for (var i = 0; i < animalList.length; i ++) {

		animalList[i].style.display = "none";
	}
	document.getElementById('preview_'+ animalName + '2').style.display = "block";
	};






function breedButtonDisplayFunction() {

	if (head && body) { 
		document.getElementById('breed_buttonID').style.display = "block";
	}
};


function breedButtonFunction() {

	var hideOnBreedList = document.getElementsByClassName("hideOnBreed")

	for (var i = 0; i < hideOnBreedList.length; i ++) {

		hideOnBreedList[i].style.display = "none";
	}
	
	document.getElementById('img_head').src = '../img/head_'+ head +'.png';

	document.getElementById('img_body').src = '../img/body_'+ body +'.png';

	window.scrollTo(0, 0);
	
	document.getElementById('AnimalName').innerHTML = "Holy cow, you breeded a rare " + head + " - " + body + "!!!";

	document.getElementById("animalDisplayDiv").classList.toggle("showAnimal");

	document.getElementById('retry_buttonID').style.display = "block";

	// document.getElementsByClassName("chart").classList.toggle("chart_show");

	document.getElementById('chartDivID').style.display = "block";

	


	// example 1
	//var x = sayHi;
	//x();


	// example2
// 	var x = function() { alert("Hi"); }
// 	x();
};


// function sayHi() {
// 	alert("hi");
// }

function reloadFunction() {
	window.location.reload(); 
};


// window.onload = sayHi;

window.onload = function() {

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'polarArea',
    data: {
        labels: ["Stubborn", "Silly", "Wacky", "Cute", "Quirky", "venomous"],
        datasets: [{
            label: '# of Votes',
            data: [13, 5, 14, 12, 18, 2],
            backgroundColor: [
                'rgba(220, 220, 220, 1)',
                'rgba(211, 211, 211, 1)',
                'rgba(192, 192, 192, 1)',
                'rgba(169, 169, 169, 1)',
                'rgba(128, 128, 128, 1)',
                'rgba(105, 105, 105, 1)'
            ],
            borderColor: [
                'rgba(211, 211, 211, 1)',
                'rgba(192, 192, 192, 1)',
                'rgba(169, 169, 169, 1)',
                'rgba(128, 128, 128, 1)',
                'rgba(105, 105, 105, 1)',
                'rgba(105, 105, 105, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
};


