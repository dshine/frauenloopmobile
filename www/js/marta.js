var translations = document.getElementsByClassName("translation")

for(var i=0; i<translations.length; i++) {
	var el = translations[i]
	el.onclick = function(evt) {
		var children = evt.target.parentElement.children
		children[0].classList.toggle("shown")
		children[1].classList.toggle("shown")
		children[2].classList.toggle("shown")
	}
}